var mboxCopyright = "Copyright 1996-2013. Adobe Systems Incorporated. All rights reserved.";




mboxUrlBuilder = function(a, b) {
 this.a = a;
 this.b = b;
 this.c = new Array();
 this.d = function(e) { return e; };
 this.f = null;
};


mboxUrlBuilder.prototype.addNewParameter = function (g, h) {
 this.c.push({name: g, value: h});
 return this;
};


mboxUrlBuilder.prototype.addParameterIfAbsent = function (g, h) {
 if (h) {
 for (var i = 0; i < this.c.length; i++) {
 var j = this.c[i];
 if (j.name === g) {
 return this;
 }
 }

 this.checkInvalidCharacters(g);
 return this.addNewParameter(g, h);
 }
};


mboxUrlBuilder.prototype.addParameter = function(g, h) {
 this.checkInvalidCharacters(g);

 for (var i = 0; i < this.c.length; i++) {
 var j = this.c[i];
 if (j.name === g) {
 j.value = h;
 return this;
 }
 }

 return this.addNewParameter(g, h);
};


mboxUrlBuilder.prototype.addParameters = function(c) {
 if (!c) {
 return this;
 }
 for (var i = 0; i < c.length; i++) {
 var k = c[i].indexOf('=');
 if (k == -1 || k == 0) {
 continue;
 }
 this.addParameter(c[i].substring(0, k),
 c[i].substring(k + 1, c[i].length));
 }
 return this;
};

mboxUrlBuilder.prototype.setServerType = function(l) {
 this.m = l;
};

mboxUrlBuilder.prototype.setBasePath = function(f) {
 this.f = f;
};


mboxUrlBuilder.prototype.setUrlProcessAction = function(n) {
 this.d = n;
};

mboxUrlBuilder.prototype.buildUrl = function() {
 var o = this.f ? this.f :
 '/m2/' + this.b + '/mbox/' + this.m;

 var p = document.location.protocol == 'file:' ? 'http:' :
 document.location.protocol;

 var e = p + "//" + this.a + o;

 var q = e.indexOf('?') != -1 ? '&' : '?';
 for (var i = 0; i < this.c.length; i++) {
 var j = this.c[i];
 e += q + encodeURIComponent(j.name) + '=' +
 encodeURIComponent(j.value);
 q = '&';
 }
 return this.r(this.d(e));
};


mboxUrlBuilder.prototype.getParameters = function() {
 return this.c;
};

mboxUrlBuilder.prototype.setParameters = function(c) {
 this.c = c;
};

mboxUrlBuilder.prototype.clone = function() {
 var s = new mboxUrlBuilder(this.a, this.b);
 s.setServerType(this.m);
 s.setBasePath(this.f);
 s.setUrlProcessAction(this.d);
 for (var i = 0; i < this.c.length; i++) {
 s.addParameter(this.c[i].name,
 this.c[i].value);
 }
 return s;
};

mboxUrlBuilder.prototype.r = function(t) {
 return t.replace(/\"/g, '&quot;').replace(/>/g, '&gt;');
};


 mboxUrlBuilder.prototype.checkInvalidCharacters = function (g) {
 var u = new RegExp('(\'|")');
 if (u.exec(g)) {
 throw "Parameter '" + g + "' contains invalid characters";
 }
 };


mboxStandardFetcher = function() { };

mboxStandardFetcher.prototype.getType = function() {
 return 'standard';
};

mboxStandardFetcher.prototype.fetch = function(v) {
 v.setServerType(this.getType());

 document.write('<' + 'scr' + 'ipt src="' + v.buildUrl() +
 '" language="JavaScript"><' + '\/scr' + 'ipt>');
};

mboxStandardFetcher.prototype.cancel = function() { };


mboxAjaxFetcher = function() { };

mboxAjaxFetcher.prototype.getType = function() {
 return 'ajax';
};

mboxAjaxFetcher.prototype.fetch = function(v) {
 v.setServerType(this.getType());
 var e = v.buildUrl();

 this.w = document.createElement('script');
 this.w.src = e;

 document.body.appendChild(this.w);
};

mboxAjaxFetcher.prototype.cancel = function() { };


mboxMap = function() {
 this.x = new Object();
 this.y = new Array();
};

mboxMap.prototype.put = function(z, h) {
 if (!this.x[z]) {
 this.y[this.y.length] = z;
 }
 this.x[z] = h;
};

mboxMap.prototype.get = function(z) {
 return this.x[z];
};

mboxMap.prototype.remove = function(z) {
 this.x[z] = undefined;
};

mboxMap.prototype.each = function(n) {
 for (var i = 0; i < this.y.length; i++ ) {
 var z = this.y[i];
 var h = this.x[z];
 if (h) {
 var A = n(z, h);
 if (A === false) {
 break;
 }
 }
 }
};


mboxFactory = function(B, b, C) {
 this.D = false;
 this.B = B;
 this.C = C;
 this.E = new mboxList();

 mboxFactories.put(C, this);

 
 
 this.F =
 typeof document.createElement('div').replaceChild != 'undefined' &&
 (function() { return true; })() &&
 typeof document.getElementById != 'undefined' &&
 typeof (window.attachEvent || document.addEventListener ||
 window.addEventListener) != 'undefined' &&
 typeof encodeURIComponent != 'undefined';
 this.G = this.F &&
 mboxGetPageParameter('mboxDisable') == null;

 var H = C == 'default';
 
 
 
 this.I = new mboxCookieManager(
 'mbox' +
 (H ? '' : ('-' + C)),
 (function() { return mboxCookiePageDomain(); })());

 
 this.G = this.G && this.I.isEnabled() &&
 (this.I.getCookie('disable') == null);
 

 if (this.isAdmin()) {
 this.enable();
 }

 this.J();
 this.K = mboxGenerateId();
 this.L = mboxScreenHeight();
 this.M = mboxScreenWidth();
 this.N = mboxBrowserWidth();
 this.O = mboxBrowserHeight();
 this.P = mboxScreenColorDepth();
 this.Q = mboxBrowserTimeOffset();
 this.R = new mboxSession(this.K,
 'mboxSession',
 'session', 31 * 60, this.I);
 this.S = new mboxPC('PC',
 1209600, this.I);

 this.v = new mboxUrlBuilder(B, b);
 this.T(this.v, H);

 this.U = new Date().getTime();
 this.V = this.U;

 var W = this;
 this.addOnLoad(function() { W.V = new Date().getTime(); });
 if (this.F) {
 
 
 this.addOnLoad(function() {
 W.D = true;
 W.getMboxes().each(function(X) {
 X.setFetcher(new mboxAjaxFetcher());
 X.finalize(); });
 });


 if (this.G) {
 this.limitTraffic(100, 10368000);
 this.Y();
 this.Z = new mboxSignaler(function(_, c) {
 return W.create(_, c);
 }, this.I);
 }

 }
};





mboxFactory.prototype.isEnabled = function() {
 return this.G;
};


mboxFactory.prototype.getDisableReason = function() {
 return this.I.getCookie('disable');
};


mboxFactory.prototype.isSupported = function() {
 return this.F;
};


mboxFactory.prototype.disable = function(ab, bb) {
 if (typeof ab == 'undefined') {
 ab = 60 * 60;
 }

 if (typeof bb == 'undefined') {
 bb = 'unspecified';
 }

 if (!this.isAdmin()) {
 this.G = false;
 this.I.setCookie('disable', bb, ab);
 }
};

mboxFactory.prototype.enable = function() {
 this.G = true;
 this.I.deleteCookie('disable');
};

mboxFactory.prototype.isAdmin = function() {
 return document.location.href.indexOf('mboxEnv') != -1;
};


mboxFactory.prototype.limitTraffic = function(cb, ab) {

};


mboxFactory.prototype.addOnLoad = function(db) {

 
 
 
 

 if (this.isDomLoaded()) {
 db();
 } else {
 var eb = false;
 var fb = function() {
 if (eb) {
 return;
 }
 eb = true;
 db();
 };

 this.gb.push(fb);

 if (this.isDomLoaded() && !eb) {
 fb();
 }
 }
};

mboxFactory.prototype.getEllapsedTime = function() {
 return this.V - this.U;
};

mboxFactory.prototype.getEllapsedTimeUntil = function(hb) {
 return hb - this.U;
};


mboxFactory.prototype.getMboxes = function() {
 return this.E;
};


mboxFactory.prototype.get = function(_, ib) {
 return this.E.get(_).getById(ib || 0);
};


mboxFactory.prototype.update = function(_, c) {
 if (!this.isEnabled()) {
 return;
 }
 if (!this.isDomLoaded()) {
 var W = this;
 this.addOnLoad(function() { W.update(_, c); });
 return;
 }
 if (this.E.get(_).length() == 0) {
 throw "Mbox " + _ + " is not defined";
 }
 this.E.get(_).each(function(X) {
 X.getUrlBuilder()
 .addParameter('mboxPage', mboxGenerateId());
 X.load(c);
 });
};


mboxFactory.prototype.setVisitorIdParameters = function(e) {
 var imsOrgId = '107E67C2524451D90A490D4C@AdobeOrg';

 if (typeof Visitor == 'undefined' || imsOrgId.length == 0) {
 return;
 }

 var visitor = Visitor.getInstance(imsOrgId);

 if (visitor.isAllowed()) {
 
 
 var addVisitorValueToUrl = function(param, getter) {
 if (visitor[getter]) {
 var callback = function(value) {
 if (value) {
 e.addParameterIfAbsent(param, value);
 }
 };
 var value = visitor[getter](callback);
 callback(value);
 }
 };

 addVisitorValueToUrl('mboxMCGVID', "getMarketingCloudVisitorID");
 addVisitorValueToUrl('mboxMCGLH', "getAudienceManagerLocationHint");
 addVisitorValueToUrl('mboxAAMB', "getAudienceManagerBlob");
 addVisitorValueToUrl('mboxMCAVID', "getAnalyticsVisitorID");
 addVisitorValueToUrl('mboxMCSDID', "getSupplementalDataID");
 }
};


mboxFactory.prototype.create = function(
 _, c, jb) {

 if (!this.isSupported()) {
 return null;
 }
 var e = this.v.clone();
 e.addParameter('mboxCount', this.E.length() + 1);
 e.addParameters(c);

 this.setVisitorIdParameters(e);

 var ib = this.E.get(_).length();
 var kb = this.C + '-' + _ + '-' + ib;
 var lb;

 if (jb) {
 lb = new mboxLocatorNode(jb);
 } else {
 if (this.D) {
 throw 'The page has already been loaded, can\'t write marker';
 }
 lb = new mboxLocatorDefault(kb);
 }

 try {
 var W = this;
 var mb = 'mboxImported-' + kb;
 var X = new mbox(_, ib, e, lb, mb);
 if (this.G) {
 X.setFetcher(
 this.D ? new mboxAjaxFetcher() : new mboxStandardFetcher());
 }

 X.setOnError(function(nb, l) {
 X.setMessage(nb);
 X.activate();
 if (!X.isActivated()) {
 W.disable(60 * 60, nb);
 window.location.reload(false);
 }


 });
 this.E.add(X);
 } catch (ob) {
 this.disable();
 throw 'Failed creating mbox "' + _ + '", the error was: ' + ob;
 }

 var pb = new Date();
 e.addParameter('mboxTime', pb.getTime() -
 (pb.getTimezoneOffset() * 60000));

 return X;
};

mboxFactory.prototype.getCookieManager = function() {
 return this.I;
};

mboxFactory.prototype.getPageId = function() {
 return this.K;
};

mboxFactory.prototype.getPCId = function() {
 return this.S;
};

mboxFactory.prototype.getSessionId = function() {
 return this.R;
};

mboxFactory.prototype.getSignaler = function() {
 return this.Z;
};

mboxFactory.prototype.getUrlBuilder = function() {
 return this.v;
};

mboxFactory.prototype.T = function(e, H) {
 e.addParameter('mboxHost', document.location.hostname)
 .addParameter('mboxSession', this.R.getId());
 if (!H) {
 e.addParameter('mboxFactoryId', this.C);
 }
 if (this.S.getId() != null) {
 e.addParameter('mboxPC', this.S.getId());
 }
 e.addParameter('mboxPage', this.K);
 e.addParameter('screenHeight', this.L);
 e.addParameter('screenWidth', this.M);
 e.addParameter('browserWidth', this.N);
 e.addParameter('browserHeight', this.O);
 e.addParameter('browserTimeOffset', this.Q);
 e.addParameter('colorDepth', this.P);


 e.addParameter('mboxXDomain', "enabled");


 e.setUrlProcessAction(function(e) {

 e += '&mboxURL=' + encodeURIComponent(document.location);
 var qb = encodeURIComponent(document.referrer);
 if (e.length + qb.length < 2000) {
 e += '&mboxReferrer=' + qb;
 }

 e += '&mboxVersion=' + mboxVersion;
 return e;
 });
};

mboxFactory.prototype.rb = function() {
 return "";
};


mboxFactory.prototype.Y = function() {
 document.write('<style>.' + 'mboxDefault'
 + ' { visibility:hidden; }</style>');
};

mboxFactory.prototype.isDomLoaded = function() {
 return this.D;
};

mboxFactory.prototype.J = function() {
 if (this.gb != null) {
 return;
 }
 this.gb = new Array();

 var W = this;
 (function() {
 var sb = document.addEventListener ? "DOMContentLoaded" : "onreadystatechange";
 var tb = false;
 var ub = function() {
 if (tb) {
 return;
 }
 tb = true;
 for (var i = 0; i < W.gb.length; ++i) {
 W.gb[i]();
 }
 };

 if (document.addEventListener) {
 document.addEventListener(sb, function() {
 document.removeEventListener(sb, arguments.callee, false);
 ub();
 }, false);

 window.addEventListener("load", function(){
 document.removeEventListener("load", arguments.callee, false);
 ub();
 }, false);

 } else if (document.attachEvent) {
 if (self !== self.top) {
 document.attachEvent(sb, function() {
 if (document.readyState === 'complete') {
 document.detachEvent(sb, arguments.callee);
 ub();
 }
 });
 } else {
 var vb = function() {
 try {
 document.documentElement.doScroll('left');
 ub();
 } catch (wb) {
 setTimeout(vb, 13);
 }
 };
 vb();
 }
 }

 if (document.readyState === "complete") {
 ub();
 }

 })();
};


mboxSignaler = function(xb, I) {
 this.I = I;
 var yb =
 I.getCookieNames('signal-');
 for (var i = 0; i < yb.length; i++) {
 var zb = yb[i];
 var Ab = I.getCookie(zb).split('&');
 var X = xb(Ab[0], Ab);
 X.load();
 I.deleteCookie(zb);
 }
};


mboxSignaler.prototype.signal = function(Bb, _ ) {
 this.I.setCookie('signal-' +
 Bb, mboxShiftArray(arguments).join('&'), 45 * 60);
};


mboxList = function() {
 this.E = new Array();
};

mboxList.prototype.add = function(X) {
 if (X != null) {
 this.E[this.E.length] = X;
 }
};


mboxList.prototype.get = function(_) {
 var A = new mboxList();
 for (var i = 0; i < this.E.length; i++) {
 var X = this.E[i];
 if (X.getName() == _) {
 A.add(X);
 }
 }
 return A;
};

mboxList.prototype.getById = function(Cb) {
 return this.E[Cb];
};

mboxList.prototype.length = function() {
 return this.E.length;
};


mboxList.prototype.each = function(n) {
 if (typeof n != 'function') {
 throw 'Action must be a function, was: ' + typeof(n);
 }
 for (var i = 0; i < this.E.length; i++) {
 n(this.E[i]);
 }
};





mboxLocatorDefault = function(g) {
 this.g = 'mboxMarker-' + g;

 document.write('<div id="' + this.g +
 '" style="visibility:hidden;display:none">&nbsp;</div>');
};

mboxLocatorDefault.prototype.locate = function() {
 var Db = document.getElementById(this.g);
 while (Db != null) {
 
 if (Db.nodeType == 1) {
 if (Db.className == 'mboxDefault') {
 return Db;
 }
 }
 Db = Db.previousSibling;
 }

 return null;
};

mboxLocatorDefault.prototype.force = function() {
 
 var Eb = document.createElement('div');
 Eb.className = 'mboxDefault';

 var Fb = document.getElementById(this.g);
 Fb.parentNode.insertBefore(Eb, Fb);

 return Eb;
};

mboxLocatorNode = function(Gb) {
 this.Db = Gb;
};

mboxLocatorNode.prototype.locate = function() {
 return typeof this.Db == 'string' ?
 document.getElementById(this.Db) : this.Db;
};

mboxLocatorNode.prototype.force = function() {
 return null;
};


mboxCreate = function(_ ) {
 var X = mboxFactoryDefault.create( _, mboxShiftArray(arguments));

 if (X) {
 X.load();
 }
 return X;
};


mboxDefine = function(jb, _ ) {
 var X = mboxFactoryDefault.create(_,
 mboxShiftArray(mboxShiftArray(arguments)), jb);

 return X;
};

mboxUpdate = function(_ ) {
 mboxFactoryDefault.update(_, mboxShiftArray(arguments));
};


mbox = function(g, Hb, v, Ib, mb) {
 this.Jb = null;
 this.Kb = 0;
 this.lb = Ib;
 this.mb = mb;
 this.Lb = null;

 this.Mb = new mboxOfferContent();
 this.Eb = null;
 this.v = v;

 
 this.message = '';
 this.Nb = new Object();
 this.Ob = 0;

 this.Hb = Hb;
 this.g = g;

 this.Pb();

 v.addParameter('mbox', g)
 .addParameter('mboxId', Hb);

 this.Qb = function() {};
 this.Rb = function() {};

 this.Sb = null;
};

mbox.prototype.getId = function() {
 return this.Hb;
};

mbox.prototype.Pb = function() {
 if (this.g.length > 250) {
 throw "Mbox Name " + this.g + " exceeds max length of "
 + "250 characters.";
 } else if (this.g.match(/^\s+|\s+$/g)) {
 throw "Mbox Name " + this.g + " has leading/trailing whitespace(s).";
 }
};

mbox.prototype.getName = function() {
 return this.g;
};


mbox.prototype.getParameters = function() {
 var c = this.v.getParameters();
 var A = new Array();
 for (var i = 0; i < c.length; i++) {
 
 if (c[i].name.indexOf('mbox') != 0) {
 A[A.length] = c[i].name + '=' + c[i].value;
 }
 }
 return A;
};


mbox.prototype.setOnLoad = function(n) {
 this.Rb = n;
 return this;
};

mbox.prototype.setMessage = function(nb) {
 this.message = nb;
 return this;
};


mbox.prototype.setOnError = function(Qb) {
 this.Qb = Qb;
 return this;
};

mbox.prototype.setFetcher = function(Tb) {
 if (this.Lb) {
 this.Lb.cancel();
 }
 this.Lb = Tb;
 return this;
};

mbox.prototype.getFetcher = function() {
 return this.Lb;
};


mbox.prototype.load = function(c) {
 if (this.Lb == null) {
 return this;
 }

 this.setEventTime("load.start");
 this.cancelTimeout();
 this.Kb = 0;

 var v = (c && c.length > 0) ?
 this.v.clone().addParameters(c) : this.v;
 this.Lb.fetch(v);

 var W = this;
 this.Ub = setTimeout(function() {
 W.Qb('browser timeout', W.Lb.getType());
 }, 15000);

 this.setEventTime("load.end");

 return this;
};


mbox.prototype.loaded = function() {
 var Vb = document.documentMode >= 10,
 Wb = this,
 Xb = function() {
 if (!Wb.activate()) {
 setTimeout(function() {
 Wb.loaded();
 }, 100);
 }
 };

 this.cancelTimeout();
 if (Vb) {
 setTimeout(Xb, 100);
 } else {
 Xb();
 }
};


mbox.prototype.activate = function() {
 if (this.Kb) {
 return this.Kb;
 }
 this.setEventTime('activate' + ++this.Ob + '.start');
 if (this.show()) {
 this.cancelTimeout();
 this.Kb = 1;
 }
 this.setEventTime('activate' + this.Ob + '.end');
 return this.Kb;
};


mbox.prototype.isActivated = function() {
 return this.Kb;
};


mbox.prototype.setOffer = function(Mb) {
 if (Mb && Mb.show && Mb.setOnLoad) {
 this.Mb = Mb;
 } else {
 throw 'Invalid offer';
 }
 return this;
};

mbox.prototype.getOffer = function() {
 return this.Mb;
};


mbox.prototype.show = function() {
 this.setEventTime('show.start');
 var A = this.Mb.show(this);
 this.setEventTime(A == 1 ? "show.end.ok" : "show.end");

 return A;
};


mbox.prototype.showContent = function(Yb) {
 if (Yb == null) {
 
 return 0;
 }
 
 
 if (this.Eb == null || !this.Eb.parentNode) {
 this.Eb = this.getDefaultDiv();
 if (this.Eb == null) {
 
 return 0;
 }
 }

 if (this.Eb != Yb) {
 this.Zb(this.Eb);
 this.Eb.parentNode.replaceChild(Yb, this.Eb);
 this.Eb = Yb;
 }

 this._b(Yb);

 this.Rb();

 
 return 1;
};


mbox.prototype.hide = function() {
 this.setEventTime('hide.start');
 var A = this.showContent(this.getDefaultDiv());
 this.setEventTime(A == 1 ? 'hide.end.ok' : 'hide.end.fail');
 return A;
};


mbox.prototype.finalize = function() {
 this.setEventTime('finalize.start');
 this.cancelTimeout();

 if (this.getDefaultDiv() == null) {
 if (this.lb.force() != null) {
 this.setMessage('No default content, an empty one has been added');
 } else {
 this.setMessage('Unable to locate mbox');
 }
 }

 if (!this.activate()) {
 this.hide();
 this.setEventTime('finalize.end.hide');
 }
 this.setEventTime('finalize.end.ok');
};

mbox.prototype.cancelTimeout = function() {
 if (this.Ub) {
 clearTimeout(this.Ub);
 }
 if (this.Lb != null) {
 this.Lb.cancel();
 }
};

mbox.prototype.getDiv = function() {
 return this.Eb;
};


mbox.prototype.getDefaultDiv = function() {
 if (this.Sb == null) {
 this.Sb = this.lb.locate();
 }
 return this.Sb;
};

mbox.prototype.setEventTime = function(ac) {
 this.Nb[ac] = (new Date()).getTime();
};

mbox.prototype.getEventTimes = function() {
 return this.Nb;
};

mbox.prototype.getImportName = function() {
 return this.mb;
};

mbox.prototype.getURL = function() {
 return this.v.buildUrl();
};

mbox.prototype.getUrlBuilder = function() {
 return this.v;
};

mbox.prototype.bc = function(Eb) {
 return Eb.style.display != 'none';
};

mbox.prototype._b = function(Eb) {
 this.cc(Eb, true);
};

mbox.prototype.Zb = function(Eb) {
 this.cc(Eb, false);
};

mbox.prototype.cc = function(Eb, dc) {
 Eb.style.visibility = dc ? "visible" : "hidden";
 Eb.style.display = dc ? "block" : "none";
};

mboxOfferContent = function() {
 this.Rb = function() {};
};

mboxOfferContent.prototype.show = function(X) {
 var A = X.showContent(document.getElementById(X.getImportName()));
 if (A == 1) {
 this.Rb();
 }
 return A;
};

mboxOfferContent.prototype.setOnLoad = function(Rb) {
 this.Rb = Rb;
};


mboxOfferAjax = function(Yb) {
 this.Yb = Yb;
 this.Rb = function() {};
};

mboxOfferAjax.prototype.setOnLoad = function(Rb) {
 this.Rb = Rb;
};

mboxOfferAjax.prototype.show = function(X) {
 var ec = document.createElement('div');

 ec.id = X.getImportName();
 ec.innerHTML = this.Yb;

 var A = X.showContent(ec);
 if (A == 1) {
 this.Rb();
 }
 return A;
};


mboxOfferDefault = function() {
 this.Rb = function() {};
};

mboxOfferDefault.prototype.setOnLoad = function(Rb) {
 this.Rb = Rb;
};

mboxOfferDefault.prototype.show = function(X) {
 var A = X.hide();
 if (A == 1) {
 this.Rb();
 }
 return A;
};

mboxCookieManager = function mboxCookieManager(g, fc) {
 this.g = g;
 
 this.fc = fc == '' || fc.indexOf('.') == -1 ? '' :
 '; domain=' + fc;
 this.gc = new mboxMap();
 this.loadCookies();
};

mboxCookieManager.prototype.isEnabled = function() {
 this.setCookie('check', 'true', 60);
 this.loadCookies();
 return this.getCookie('check') == 'true';
};


mboxCookieManager.prototype.setCookie = function(g, h, ab) {
 if (typeof g != 'undefined' && typeof h != 'undefined' &&
 typeof ab != 'undefined') {
 var hc = new Object();
 hc.name = g;
 hc.value = escape(h);
 
 hc.expireOn = Math.ceil(ab + new Date().getTime() / 1000);
 this.gc.put(g, hc);
 this.saveCookies();
 }
};

mboxCookieManager.prototype.getCookie = function(g) {
 var hc = this.gc.get(g);
 return hc ? unescape(hc.value) : null;
};

mboxCookieManager.prototype.deleteCookie = function(g) {
 this.gc.remove(g);
 this.saveCookies();
};

mboxCookieManager.prototype.getCookieNames = function(ic) {
 var jc = new Array();
 this.gc.each(function(g, hc) {
 if (g.indexOf(ic) == 0) {
 jc[jc.length] = g;
 }
 });
 return jc;
};

mboxCookieManager.prototype.saveCookies = function() {
 var kc = false;
 var lc = 'disable';
 var mc = new Array();
 var nc = 0;
 this.gc.each(function(g, hc) {
 if(!kc || g === lc) {
 mc[mc.length] = g + '#' + hc.value + '#' +
 hc.expireOn;
 if (nc < hc.expireOn) {
 nc = hc.expireOn;
 }
 }
 });

 var oc = new Date(nc * 1000);
 document.cookie = this.g + '=' + mc.join('|') +
 
 '; expires=' + oc.toGMTString() +
 '; path=/' + this.fc;
};

mboxCookieManager.prototype.loadCookies = function() {
 this.gc = new mboxMap();
 var pc = document.cookie.indexOf(this.g + '=');
 if (pc != -1) {
 var qc = document.cookie.indexOf(';', pc);
 if (qc == -1) {
 qc = document.cookie.indexOf(',', pc);
 if (qc == -1) {
 qc = document.cookie.length;
 }
 }
 var rc = document.cookie.substring(
 pc + this.g.length + 1, qc).split('|');

 var sc = Math.ceil(new Date().getTime() / 1000);
 for (var i = 0; i < rc.length; i++) {
 var hc = rc[i].split('#');
 if (sc <= hc[2]) {
 var tc = new Object();
 tc.name = hc[0];
 tc.value = hc[1];
 tc.expireOn = hc[2];
 this.gc.put(tc.name, tc);
 }
 }
 }
};


mboxSession = function(uc, vc, zb, wc,
 I) {
 this.vc = vc;
 this.zb = zb;
 this.wc = wc;
 this.I = I;

 this.xc = false;

 this.Hb = typeof mboxForceSessionId != 'undefined' ?
 mboxForceSessionId : mboxGetPageParameter(this.vc);

 if (this.Hb == null || this.Hb.length == 0) {
 this.Hb = I.getCookie(zb);
 if (this.Hb == null || this.Hb.length == 0) {
 this.Hb = uc;
 this.xc = true;
 }
 }

 I.setCookie(zb, this.Hb, wc);
};


mboxSession.prototype.getId = function() {
 return this.Hb;
};

mboxSession.prototype.forceId = function(yc) {
 this.Hb = yc;

 this.I.setCookie(this.zb, this.Hb, this.wc);
};


mboxPC = function(zb, wc, I) {
 this.zb = zb;
 this.wc = wc;
 this.I = I;

 this.Hb = typeof mboxForcePCId != 'undefined' ?
 mboxForcePCId : I.getCookie(zb);
 if (this.Hb != null) {
 I.setCookie(zb, this.Hb, wc);
 }

};


mboxPC.prototype.getId = function() {
 return this.Hb;
};


mboxPC.prototype.forceId = function(yc) {
 if (this.Hb != yc) {
 this.Hb = yc;
 this.I.setCookie(this.zb, this.Hb, this.wc);
 return true;
 }
 return false;
};

mboxGetPageParameter = function(g) {
 var A = null;
 var zc = new RegExp(g + "=([^\&]*)");
 var Ac = zc.exec(document.location);

 if (Ac != null && Ac.length >= 2) {
 A = Ac[1];
 }
 return A;
};

mboxSetCookie = function(g, h, ab) {
 return mboxFactoryDefault.getCookieManager().setCookie(g, h, ab);
};

mboxGetCookie = function(g) {
 return mboxFactoryDefault.getCookieManager().getCookie(g);
};

mboxCookiePageDomain = function() {
 var fc = (/([^:]*)(:[0-9]{0,5})?/).exec(document.location.host)[1];
 var Bc = /[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/;

 if (!Bc.exec(fc)) {
 var Cc =
 (/([^\.]+\.[^\.]{3}|[^\.]+\.[^\.]+\.[^\.]{2})$/).exec(fc);
 if (Cc) {
 fc = Cc[0];
 }
 }

 return fc ? fc: "";
};

mboxShiftArray = function(Dc) {
 var A = new Array();
 for (var i = 1; i < Dc.length; i++) {
 A[A.length] = Dc[i];
 }
 return A;
};

mboxGenerateId = function() {
 return (new Date()).getTime() + "-" + Math.floor(Math.random() * 999999);
};

mboxScreenHeight = function() {
 return screen.height;
};

mboxScreenWidth = function() {
 return screen.width;
};

mboxBrowserWidth = function() {
 return (window.innerWidth) ? window.innerWidth :
 document.documentElement ? document.documentElement.clientWidth :
 document.body.clientWidth;
};

mboxBrowserHeight = function() {
 return (window.innerHeight) ? window.innerHeight :
 document.documentElement ? document.documentElement.clientHeight :
 document.body.clientHeight;
};

mboxBrowserTimeOffset = function() {
 return -new Date().getTimezoneOffset();
};

mboxScreenColorDepth = function() {
 return screen.pixelDepth;
};
/* patch IE */
if (!Array.prototype.indexOf) { 
    Array.prototype.indexOf = function(obj, start) {
         for (var i = (start || 0), j = this.length; i < j; i++) {
             if (this[i] === obj) { return i; }
         }
         return -1;
    }
}
if (!window.CQ_Analytics) {
    window.CQ_Analytics = {};
}

if(typeof CQ == 'undefined'){
CQ = window.CQ;
}

// TODO is there a better way?
if (typeof CQ_Analytics.TestTarget !== 'undefined') {
    var oldTandT = CQ_Analytics.TestTarget;
}

CQ_Analytics.TestTarget = new function() {


    /**
     * Extracts the possible mbox parameters from an object, by looking for entry similar to
     *
     * {
     *   'name'  : 'mbox'
     *   'value' : '...'
     * }
     *
     * @param {Object} the object to inspect for mbox parameters
     */
    function extractMboxParameters(obj, parent) {

        if (!obj) {
            return;
        }

        // see http://stackoverflow.com/a/120280/112671, we want to skip traversal of DOM elements
        if ('nodeType' in obj) {
            return;
        }

        var key, value, parentKey, parentEntry;

        if (obj.hasOwnProperty('name') && obj.hasOwnProperty('value')) {
            for (parentItem in parent) {
                var parentEntry = parent[parentItem];
                if (typeof parentEntry === 'object') {
                    if (parentEntry.hasOwnProperty('name') && parentEntry['name'] == 'mbox') {
                        return parent;
                    }
                }
            }
            return parent;
        }

        for (key in obj) {
            if (!obj.hasOwnProperty(key)) {
                continue;
            }

            var newObj = obj[key];
            if (typeof newObj === 'object') {
                var extracted = extractMboxParameters(newObj, obj);
                if (extracted) {
                    return extracted;
                }
            }
        }

    }

    function isEditOrPreview() {
        var $COOKIE = (document.cookie || '').split(/;\s*/).reduce(function(re, c) {
            var tmp = c.match(/([^=]+)=(.*)/);
            if (tmp) re[tmp[1]] = unescape(tmp[2]);
            return re;
        }, {});

        return (typeof $COOKIE["wcmmode"] == "undefined"
                    || $COOKIE["wcmmode"] == "preview"
                    || $COOKIE["wcmmode"] == "edit");
    }

    function getCookieObject() {
        var $COOKIE = (document.cookie || '').split(/;\s*/).reduce(function(re, c) {
            var tmp = c.match(/([^=]+)=(.*)/);
            if (tmp) re[tmp[1]] = unescape(tmp[2]);
            return re;
        }, {});

        return $COOKIE;
    }

    function pullContent(path) {
        // if possible, force WCM mode as disabled to prevent edit decorations from being applied
        var wcmmode = CQ.shared.HTTP.getParameter(document.location.href, 'wcmmode');
        if (typeof CQ.WCM !== 'undefined') {
            wcmmode = "disabled";
        }
        if (wcmmode && wcmmode.length > 0) {
            path = CQ.shared.HTTP.addParameter(path, 'wcmmode', wcmmode);
        }

        // pull in the content
        var output = CQ.shared.HTTP.get(path);
        var isOk = (output && output.status && output.status == 200);
        var hasBody = (output && output.body && output.body.length > 0);

        if (isOk && hasBody) {
            return output;
        } else {
            if (console) console.log("Could not pull resource. Response[status:{},body:{}]", output.status, output.body);
            return null;
        }
    }

    return {

        lateMboxArrivalTimeouts: {},

        /**
         * Initialises the Adobe Target integration
         *
         * It reads the clientcode from either the clientcode parameter (deprecated) or from
         * the CQ_Analytics.TestTarget.clientCode variable (preferred).
         *
         * @param {String} clientcode the optional client code
         */
        init: function(clientcode) {

            if (CQ_Analytics.TestTarget.clientCode) {
                clientcode = CQ_Analytics.TestTarget.clientCode;
            } else {
                CQ_Analytics.TestTarget.clientCode = clientcode;
            }

            if (clientcode) {

                CQ_Analytics.TestTarget.clientCode = clientcode;

                var server = clientcode + '.tt.omtrdc.net';
                if (typeof mboxVersion == 'undefined') {
                    // Cognifide: change required for shared segments integration
                    mboxVersion = 48;
                    mboxFactories = new mboxMap();
                    mboxFactoryDefault = new mboxFactory(server, clientcode, 'default');
                }

                // this diverges from the standard T&T mbox, but client-side simulation breaks if we don't do this
                // the reason is that T&T will set and 'mboxSession' cookie when mboxXDomain == true, which we
                // can't override

                if (this.isInSimulationMode()) {
                    mboxFactories.each(function(mboxFactoryName) {
                        var mboxFactory = mboxFactories.get(mboxFactoryName);
                        mboxFactory.getUrlBuilder().addParameter("mboxXDomain", "disabled");
                    });
                }


                if (mboxGetPageParameter("mboxDebug") != null
                    || mboxFactoryDefault.getCookieManager().getCookie("debug") != null) {
                    setTimeout(
                        function() {
                            if (typeof mboxDebugLoaded == 'undefined') {
                                alert('Could not load the remote debug.\nPlease check your connection to Adobe Target servers');
                            }
                        }, 60 * 60);
                    document.write('<'
                        + 'scr'
                        + 'ipt language="Javascript1.2" src='
                        + '"http://admin4.testandtarget.omniture.com/admin/mbox/mbox_debug.jsp?mboxServerHost='
                        + server + '&clientCode=' + clientcode + '"><'
                        + '\/scr' + 'ipt>');
                }


                mboxVizTargetUrl = function(_) {
                    if (!mboxFactoryDefault.isEnabled()) {
                        return;
                    }

                    var v = mboxFactoryDefault.getUrlBuilder().clone();
                    v.setBasePath('/m2/' + clientcode + '/viztarget');

                    v.addParameter('mbox', _);
                    v.addParameter('mboxId', 0);
                    v.addParameter('mboxCount',
                        mboxFactoryDefault.getMboxes().length() + 1);

                    var pb = new Date();
                    v.addParameter('mboxTime', pb.getTime() -
                        (pb.getTimezoneOffset() * 60000));

                    v.addParameter('mboxPage', mboxGenerateId());

                    var c = mboxShiftArray(arguments);
                    if (c && c.length > 0) {
                        v.addParameters(c);
                    }

                    v.addParameter('mboxDOMLoaded', mboxFactoryDefault.isDomLoaded());

                    mboxFactoryDefault.setVisitorIdParameters(v);

                    return v.buildUrl();
                };
            }

            /**
             * Skip setting the offer for updates arriving too late
             */
            mbox.prototype._setOffer = mbox.prototype.setOffer;
            mbox.prototype.setOffer = function(Nb) {

                // TNT-17129
                if ('_onLoad' in Nb) {
                    Nb.show = function(_mbox) {
                        var _defaultDiv = _mbox.getDefaultDiv();
                        if (_defaultDiv == null) { // this needs to be '==' ,  see CQ5-28207
                            return 0;
                        }

                        var _mboxName = _mbox.getId() ? _mbox.getId() : _mbox.getName();

                        // modification - instead of overriding the onclick element
                        // to look for the the clickDiv which _should_ be present in the
                        // DOM right now instead of looking for it when the div is clicked
                        var _clickDiv = document.getElementById('mboxClick-' + _mboxName);
                        if (_clickDiv && _clickDiv.onclick) {
                            _defaultDiv.onclick = _clickDiv.onclick;
                        }

                        var _result = _mbox.hide();
                        if (_result == 1) {
                            this._onLoad();
                        }
                        return _result;
                    };
                }

                var _mboxName = this.g;
                CQ_Analytics.TestTarget.clearLateMboxArrivalTimeout(_mboxName);

                if (CQ_Analytics.TestTarget.ignoredUpdates[_mboxName]) {
                    delete CQ_Analytics.TestTarget.ignoredUpdates[_mboxName];
                    return this;
                } else {
                    return this._setOffer(Nb);
                }
            };

            /**
             * Removes the default div from this mbox
             *
             * <p>This function is useful when the DOM on the page changes to prevent the cached DOM element from being
             * a disconnected from the actual document.</p>
             */
                // TNT-16921
            mbox.prototype.clearDefaultDiv = function() {
                this.Sb = null;
            };

            mbox.prototype.setUrlBuilder = function(urlBuilder) {
                this.v = urlBuilder;
                this.v.addParameter('mbox', this.g)
                    .addParameter('mboxId', this.Hb);
            };

            mboxCookieManager.prototype.deleteAllCookies = function() {

                var that = this;

                this.gc.each(function(cookieName, cookie) {
                    that.deleteCookie(cookieName);
                });
            };

            mboxFactory.prototype.regenerateSession = function() {

                this.I.deleteAllCookies();
                this.K = mboxGenerateId();


                this.R = new mboxSession(this.K,
                    'mboxSession',
                    'session', 31 * 60, this.I);
                this.S = new mboxPC('PC',
                    1209600, this.I);

                // TODO need to remove reference to CQ_Analytics.TestTarget to submit this upstream; should be a property of the mboxFactory
                this.v = new mboxUrlBuilder(this.B, CQ_Analytics.TestTarget.clientCode);
                this.T(this.v, this.H);
                // see comment in 'init' about the need to set this
                this.v.addParameter('mboxXDomain', 'disabled');

                var that = this;
                this.getMboxes().each(function(mbox) {
                    mbox.setUrlBuilder(that.v.clone());
                });
            };

            mboxOfferDefault.prototype._show = mboxOfferDefault.prototype.show;
            mboxOfferDefault.prototype.show = function(content) {
                var cookiez = getCookieObject();
                if (cookiez["wcmmode"] === "edit"
                    && cookiez["cq-authoring-mode"] === "TOUCH") {
                    var defaultDiv = content.getDefaultDiv();

                    if (defaultDiv.parentNode) {
                        var e = defaultDiv.parentNode.getElementsByTagName("cq")[0];
                        // pull the default content from the repo
                        var defaultContent = pullContent(e.getAttribute("data-path") + ".default.html");
                        if (defaultContent !== null) {
                            // put the new content in the DOM
                            var scriptWrapper = document.createElement("div");
                            scriptWrapper.innerHTML = defaultContent.body;
                            defaultDiv.parentNode.replaceChild(scriptWrapper, defaultDiv);
                            // run the scripts that we may have in the new content
                            var scripts = defaultDiv.getElementsByTagName("script");
                            for (var i = 0; i < scripts.length; i++) {
                                eval(scripts[i].text);
                            }
                        }
                    }
                } else {
                    this._show(content);
                }

            }

        },
        /**
         * Fetches the resource from provided path and writes the
         * response to the document or the mbox Element if
         *
         * <ul>
         * <li>response status code is 200</li>
         * <li>response has a body with length > 0</li>
         * </ul>
         *
         * Uses a synchronous call for requesting the resource. If a WCM mode is defined this
         * call forces the resource to be rendered with WCM mode disabled.
         *
         * @static
         * @param {String}
         *            path Path to document/node to request.
         */
        pull: function(path) {

            // if possible, force WCM mode as disabled to prevent edit decorations from being applied
            var wcmmode = CQ.shared.HTTP.getParameter(document.location.href, 'wcmmode');
            if (typeof CQ.WCM !== 'undefined') {
                wcmmode = "disabled";
            }
            if (wcmmode && wcmmode.length > 0) {
                path = CQ.shared.HTTP.addParameter(path, 'wcmmode', wcmmode);
            }

            // pull in the content
            var output = CQ.shared.HTTP.get(path);
            var isOk = (output && output.status && output.status == 200);
            var hasBody = (output && output.body && output.body.length > 0);

            if (isOk && hasBody) {
                var caller = arguments.callee.caller;
                var outputWritten = false;

                // the target is the div used for the default content
                var target;

                // try and the detect the caller parameters by inspecting the caller stack 
                while (caller) {
                    if (caller.arguments.length > 0) {

                        var mboxParameters = extractMboxParameters(caller.arguments[0]);
                        if (!mboxParameters) {
                            continue;
                        }

                        var mboxName, mboxId, i, entry;

                        for (i = 0; i < mboxParameters.length; i++) {
                            entry = mboxParameters[i];
                            if ('name' in entry && 'value' in entry) {
                                if (entry['name'] === 'mbox') {
                                    mboxName = entry['value'];
                                } else if (entry['name'] === 'mboxId') {
                                    mboxId = entry['value'];
                                }
                            }
                        }


                        target = document.getElementById("mboxImported-default-" + mboxName + "-" + mboxId);
                        break;
                    }
                    caller = caller.arguments.callee.caller;
                }
                ;

                // if a target is found, process
                if (target) {
                    // look for the wrapper div which tracks clicks
                    var childDivs = target.getElementsByTagName('div');
                    if (childDivs.length == 1) {
                        target = childDivs[0];
                    }

                    var scriptwrapper = document.createElement('div');
                    scriptwrapper.innerHTML = output.body;
                    target.appendChild(scriptwrapper);
                    var scripts = target.getElementsByTagName('script');
                    for (var i = 0; i < scripts.length; i++) {
                        eval(scripts[i].text);
                    }

                    outputWritten = true;
                }

                // CQ-15679 - fallback in case we don't find a target div
                if (!outputWritten) {
                    document.write(output.body);
                }
            } else {
                if (console) console.log("Could not pull resource. Response[status:{},body:{}]", output.status, output.body);
            }
        },

        /**
         * Triggers an update of all the registered mboxes
         *
         * <p>Delays the update requests based on the <tt>delay</tt> parameter so that multiple update requests
         * are clumped together.</p>
         *
         * @param delay {Integer} the delay in milliseconds to apply to the reload, defaults to 500
         */
        triggerUpdate: function(delay) {

            if (typeof delay == "undefined")
                delay = 500;

            if (!CQ_Analytics.TestTarget.reloadRequested) {
                CQ_Analytics.TestTarget.reloadRequested = true;
                setTimeout(function() {
                        CQ_Analytics.TestTarget.deleteMboxCookies();
                        CQ_Analytics.TestTarget.reloadRequested = false;
                    },
                    delay);
            }
        },

        registerMboxUpdateCalls: function() {
            if (CQ_Analytics.mboxes) {
                CQ_TestTarget = {};
                CQ_TestTarget.usedStoresLoaded = false;
                CQ_TestTarget.usedStores = CQ_Analytics.TestTarget.getMappedSessionstores();

                var trackStoreUpdate = function(sessionstore) {
                    var idx = $CQ.inArray(sessionstore.getName(), CQ_TestTarget.usedStores);
                    if (idx > -1 && !$CQ.isEmptyObject(sessionstore.getData())) {
                        CQ_TestTarget.usedStores.splice(idx, 1);
                    }
                    if (CQ_TestTarget.usedStores.length < 1 && !CQ_TestTarget.usedStoresLoaded) {
                        var campaignStore = ClientContext.get("campaign");
                        if (campaignStore && campaignStore.isCampaignSelected()) {
                            return;
                        }
                        CQ_Analytics.TestTarget.callMboxUpdate();
                        CQ_TestTarget.usedStoresLoaded = true;
                    }
                };

                if (CQ_TestTarget.usedStores.length > 0) {

                    // iterate over a copy of the stores since trackStoreUpdate potentially
                    // modifies the CQ_TestTarget.usedStores array
                    var usedStoresCopy = CQ_TestTarget.usedStores.slice(0);

                    // 1. handle stores which are already initialized
                    for (var i = 0; i < usedStoresCopy.length; i++) {
                        var storeName = usedStoresCopy[i];
                        var sessionstore = CQ_Analytics.ClientContextMgr.getRegisteredStore(storeName);
                        if (sessionstore.isInitialized()) {
                            trackStoreUpdate(sessionstore);
                        }
                    }

                    // 2. handle stores which are not initialized but trigger events 
                    CQ_Analytics.CCM.addListener("storeupdate", function(e, sessionstore) {
                        trackStoreUpdate(sessionstore);
                    });
                    // fallback in case the store does not call storeupdate 
                    CQ_Analytics.CCM.addListener("storesinitialize", function(e, sessionstore) {
                        if (!CQ_TestTarget.usedStoresLoaded) {
                            var campaignStore = ClientContext.get("campaign");
                            if (campaignStore && campaignStore.isCampaignSelected()) {
                                return;
                            }

                            CQ_Analytics.TestTarget.callMboxUpdate();
                        }
                    });
                } else {

                    var campaignStore = ClientContext.get("campaign");
                    if (campaignStore && campaignStore.isCampaignSelected()) {
                        return;
                    }
                    CQ_Analytics.TestTarget.callMboxUpdate();
                }
            }
        },

        maxProfileParams: 200,

         callMboxUpdate: function() {
            if (CQ_Analytics.mboxes) {
                for (var i = 0; i < CQ_Analytics.mboxes.length; i++) {
                    var updateArgs = [CQ_Analytics.mboxes[i].name];
                    var profileParams = 0;
                    if (!CQ_Analytics.mboxes[i].defined) {
                        var callParameters = [CQ_Analytics.mboxes[i].id,CQ_Analytics.mboxes[i].name];
                        mboxDefine.apply(undefined, callParameters.concat(CQ_Analytics.mboxes[i].staticParameters));
                        CQ_Analytics.mboxes[i].defined = true;
                    }
                    for (var j = 0; j < CQ_Analytics.mboxes[i].mappings.length; j++) {
                        var profileprefix = "";
                        var param = CQ_Analytics.mboxes[i].mappings[j].param;
                        var keypath = '/' + CQ_Analytics.mboxes[i].mappings[j].ccKey.replace('.', '/');
                        if (CQ_Analytics.mboxes[i].isProfile.indexOf(param) > -1) {
                            if (CQ_Analytics.TestTarget.maxProfileParams > 0 && ++profileParams > CQ_Analytics.TestTarget.maxProfileParams) {
                                mboxUpdate.apply(this, updateArgs);
                                updateArgs = [CQ_Analytics.mboxes[i].name];
                                profileParams = 0;
                            }
                            /* we should always apply the prefix, to prevent parameter name collisions */
                            /*if (!param.match(/^profile\..*$/)) {*/
                            profileprefix = "profile.";
                            /*}*/
                        }
                        updateArgs.push(profileprefix + param + "=" + CQ_Analytics.Variables.replaceVariables(CQ_Analytics.ClientContext.get(keypath)));
                    }

                    if (CQ_Analytics.mboxes[i].includeResolvedSegments && CQ_Analytics.SegmentMgr) {
                        var resolvedSegments = CQ_Analytics.SegmentMgr.getResolved();
                        if (resolvedSegments.length > 0) {
                            updateArgs.push('profile._cq_.resolvedSegments=|' + CQ_Analytics.SegmentMgr.getResolved().join('|') + '|');
                        }
                    }
                    /* space out the first call, which is probably the global mbox, by 100 ms,
                     * to give T&T time to process the profile and use it in the next update calls
                     */
                    var that = this;
                    (function(args) {
                        setTimeout(function() {
                            mboxUpdate.apply(that, args)
                        }, (i > 0 ? 100 : 0));
                    })(updateArgs);
                }
            }
        },

        /**
         * Returns an Array of session store names used in
         * CQ_Analytics.mboxes mappings. Returns empty Array if none
         * found.
         */
        getMappedSessionstores: function() {
            var storenames = [];
            if (CQ_Analytics.mboxes) {
                for (var i = 0; i < CQ_Analytics.mboxes.length; i++) {
                    for (var j = 0; j < CQ_Analytics.mboxes[i].mappings.length; j++) {
                        var mapping = CQ_Analytics.mboxes[i].mappings[j].ccKey;
                        var tmp = mapping.split(".");
                        var storename = tmp[0];
                        var key = tmp[1];
                        if ($CQ.inArray(storename, storenames) < 0) {
                            storenames.push(storename);
                        }
                    }
                }
            }
            return storenames;
        },

        /**
         * Returns true if the mbox calls are to me made in simulation mode ( WCM preview and edit modes )
         */
        isInSimulationMode: function() {
            // use CQ.WCM when available
            if (CQ && CQ.WCM) {
                return CQ.WCM.isPreviewMode() || CQ.utils.WCM.isEditMode();
            }
            // fallback to reading the cookies directly
            return isEditOrPreview();
        },

        /**
         * Deletes mbox cookies. If mboxFactoryDefault is undefined the function returns.
         * Forces an mbox update if either of CQ.WCM.isPreviewMode() or CQ.utils.WCM.isEditMode() is true.
         */
        deleteMboxCookies: function() {
            if (typeof mboxFactoryDefault == 'undefined') return;

            if (this.isInSimulationMode()) {
                mboxFactoryDefault.regenerateSession();

                var campaignStore = ClientContext.get("campaign");
                if (campaignStore && !campaignStore.isCampaignSelected()) {
                    return;
                }
                CQ_Analytics.TestTarget.callMboxUpdate();
            }
        },

        registerListeners: function() {
            var stores = CQ_Analytics.CCM.getStores();
            for (var storename in stores) {
                var store = stores[storename];
                // completely ignore the mouse store since it's never useful for T&T
                if (storename != "mouseposition" && store.addListener) {
                    store.addListener("update", function(event, property) {
                        // avoid the surferstore getting mouse position updates
                        if (typeof property == 'undefined' ||
                            ( property && property.match && property.match("^mouse") != "mouse"))
                            CQ_Analytics.TestTarget.triggerUpdate();
                    });
                }
            }
        },

        ignoredUpdates: {},

        ignoreNextUpdate: function(mboxName) {
            CQ_Analytics.TestTarget.ignoredUpdates[mboxName] = true;
        },

        /**
         * Adds a new mboxDefinition to the CQ_Analytics.mboxes array
         *
         * <p>Removes any mbox definition with the same mbox id prior to adding the passed
         * mboxDefinition.</p>
         *
         * @return {Boolean} true if an mbox was replaced, false otherwise
         */
        addMbox: function(mboxDefinition) {
            var replaced = false;

            if (!CQ_Analytics.mboxes) {
                CQ_Analytics.mboxes = [];
            }
            for (var i = 0; i < CQ_Analytics.mboxes.length; i++) {

                var mbox = CQ_Analytics.mboxes[i];
                //  cleanup existing mbox
                if (mbox.id == mboxDefinition.id) {
                    CQ_Analytics.mboxes.splice(i, 1);
                    replaced = true;
                    break;
                }
            }

            CQ_Analytics.mboxes.push(mboxDefinition);

            return replaced;
        },

        /**
         * Hides the default content for an mbox
         */
        hideDefaultMboxContent: function(mboxId) {
            $CQ('#' + mboxId).find('div').css('visibility', 'hidden');
        },

        /**
         * Shows the default content for an mbox
         */
        showDefaultMboxContent: function(mboxId, mboxName) {
            var defaultContent = $CQ('#' + mboxId);

            // defaultContent no longer present -> mbox has loaded 
            if (!defaultContent.length)
                return;

            //need to define the mbox now, if it's not already defined

            CQ_Analytics.mboxes.map(function(m) {
                if (m.name === mboxName && !m.defined) {
                    var callParameters = [m.id,m.name];
                    mboxDefine.apply(undefined, callParameters.concat(m.staticParameters));
                    m.defined = true;
                }
            });

            mboxFactoryDefault.get(mboxName).show(new mboxOfferDefault());
            CQ_Analytics.TestTarget.ignoreNextUpdate(mboxName);
        },

        ignoreLateMboxArrival: function(mboxId, mboxName, timeout) {

            this.clearLateMboxArrivalTimeout(mboxId);

            var that = this;
            this.lateMboxArrivalTimeouts[mboxId] = setTimeout(function() {
                that.showDefaultMboxContent(mboxId, mboxName);
                that.clearLateMboxArrivalTimeout(mboxId);
            }, timeout);
        },

        clearLateMboxArrivalTimeout: function(mboxId) {
            if (this.lateMboxArrivalTimeouts[mboxId]) {
                clearTimeout(this.lateMboxArrivalTimeouts[mboxId]);
                delete this.lateMboxArrivalTimeouts[mboxId];
            }
        }
    }
};

// restore previous attributes
if (typeof oldTandT !== 'undefined ') {
    for (var prop in oldTandT) {
        CQ_Analytics.TestTarget[prop] = oldTandT[prop];
    }
}
CQ_Analytics.TestTarget.init();

