(function ($) {
	"use strict";


	var api = {};

	api.onRegister = function (scope) {
		var $nav = scope.$scope,
        	mobileButton = $nav.find('.navigation-mobile-menu'),
			navigationRoot = $nav.find('.navigation-branch.navigation-level1');

        $('<span/><span/><span/><span/>').appendTo(mobileButton);

        mobileButton.on('tap click', function(){
        	navigationRoot.toggleClass('open');
        	$nav.toggleClass('open');
            $(this).toggleClass('open');
		});
	};

	Cog.registerComponent({
		name: "navigation",
		api: api,
		selector: ".navigation"
	});

	return api;
}(Cog.jQuery()));

