/**
 * Video Gallery
 */

(function ($) {
	"use strict";

	var api = {},
		refs = {
			componentName: "videoGallery",
			thumbnailsContainerSelector: ".videoGallery-thumbnails",
			thumbnailsListContainerSelector: ".videoGallery-list",
			thumbnailsArrowsSelector: ".videoGallery-arrow",
			descriptionSelector: ".videoGallery-description",
			titleSelector: ".videoGallery-heading",
			videoWrapper: ".videoGallery-video"
		};

	api.onRegister = function (scope) {

		var $this = scope.$scope,
			$video = $this.find("video"),

			$thumbnailsContainer = $this.find(refs.thumbnailsContainerSelector),
			$thumbnailsListContainer =
				$thumbnailsContainer.find(refs.thumbnailsListContainerSelector),
			$arrows = $this.find(refs.thumbnailsArrowsSelector),
			listContainerWidth = $thumbnailsListContainer.width(),
			$descriptionContainer = $this.find(refs.descriptionSelector),
			$titleContainer = $this.find(refs.titleSelector),
			$thumbnailsList = $thumbnailsListContainer.find("ul"),
			$thumbnailsItems = $thumbnailsList.find("li"),
			thumbnailWidth = $thumbnailsItems.eq(0).outerWidth(true),
			listOuterWidth = $thumbnailsItems.length * thumbnailWidth,
			autoPlayNext = $this.find(refs.videoWrapper).data("autoplay-next-video");

		//set width od container based of amount of thumbnails
		$thumbnailsList.css("width", listOuterWidth);


		new MediaElementPlayer($video[0], {
			features: ["playpause", "progress", "current",
				"duration", "tracks", "volume", "fullscreen"],
			enableAutosize: true,
			plugins: ["flash"],
			pluginPath: api.external.settings.themePath + "/assets/swf/",
			flashName: "flashmediaelement.swf",
			success: function (mediaElement) {
				//set and load new source to player
				function reloadVideo(data) {
					data.mediaElement.pause();

					//change video, base on current format
					data.mediaElement.setSrc(data.src);
					data.mediaElement.load();

					if (data.poster) {
						var $mediaElement = $(data.mediaElement);
						$mediaElement.attr("poster", data.poster);
						$mediaElement.parents(".mejs-inner").find(".mejs-poster").css({
							"background-image": "url('" + data.poster + "')",
							"display": "block"
						});
					}

					if (data.autoplay) {
						data.mediaElement.play();
					}

					//change texts of title and description
					$descriptionContainer.text(data.description);
					$titleContainer.text(data.title);
				}

				//on click at thumbnail, load new video
				$thumbnailsItems.on("click", function () {
					var $this = $(this),
						src = provideArrayOfSources($this),
						title = $this.data("title"),
						description = $this.data("description"),
						poster = $this.find("img").attr("src");

					reloadVideo({
						mediaElement: mediaElement,
						src: src,
						autoplay: true,
						title: title,
						poster: poster,
						description: description
					});

					//change active state to clicked thumbnail
					$thumbnailsItems.removeClass("is-active");
					$this.addClass("is-active");
				});

				//after finished video, play the next one from thumbnails list
				mediaElement.addEventListener("ended", function () {

					//get next video data
					var $nextThumbnail = $thumbnailsItems.filter(".is-active").eq(0).next(),
						src = provideArrayOfSources($nextThumbnail),
						title = $nextThumbnail.data("title"),
						description = $nextThumbnail.data("description"),
						poster = $nextThumbnail.find("img").attr("src");

					//only if current video is not last on
					// thumbnails list - load the new one
					if ($nextThumbnail.length) {
						reloadVideo({
							mediaElement: mediaElement,
							src: src,
							autoplay: autoPlayNext,
							title: title,
							poster: poster,
							description: description
						});

						//re-set active states
						$thumbnailsItems.removeClass("is-active");
						$nextThumbnail.addClass("is-active");
					}
				});

				function recalculateListWith() {
					listContainerWidth = $thumbnailsListContainer.width();
					$thumbnailsList.css({left: 0});
					$arrows.filter(".videoGallery-arrow-left").addClass("is-disabled");
					$arrows.filter(".videoGallery-arrow-right").removeClass("is-disabled");
				}

				$(window).resize(_.throttle(recalculateListWith, 150));

				//scrolling logic
				$arrows.click("click", function () {
					//stop previous animation if exists
					$thumbnailsList.stop(false, true);

					var $this = $(this),
					//parse left css property
						left = parseInt($thumbnailsList.css("left"), 10),

					//1 - right, -1 - left
						vector = $this.hasClass("videoGallery-arrow-left") ? 1 : -1,

					//calculate next position of list
						nextLeft = left + (thumbnailWidth * vector),

					//calculate position of the end of list
						maxLeft = listOuterWidth - listContainerWidth +
							(listContainerWidth % thumbnailWidth);

					//reset states
					$arrows.removeClass("is-disabled");

					//check if list didn't reach the beginning or end
					if (nextLeft <= 0 && (-1) * nextLeft <= maxLeft) {
						$thumbnailsList.animate({left: nextLeft});
					} else {
						//disable clicked arrow
						$this.addClass("is-disabled");
					}

					//check if this is last possible move
					nextLeft += thumbnailWidth * vector;
					if (!(nextLeft <= 0 && (-1) * nextLeft <= maxLeft)) {
						//disable clicked arrow
						$this.addClass("is-disabled");
					}
				});
			}
		});


	};

	function provideArrayOfSources(domElement) {
		var data = $(domElement).data(),
		// build array of sources and types
			sources = [
				{src: data.srcMp4, type: "video/mp4"},
				{src: data.srcOgg, type: "video/ogg"},
				{src: data.srcWebm, type: "video/webm"}
			];
		// remove item if source is falsy
		return _.filter(sources, "src");
	}

	Cog.registerComponent({
		name: "videoGallery",
		api: api,
		selector: ".videoGallery",
		requires: [
			{
				name: "utils.settings",
				apiId: "settings"
			}
		]
	});

}(Cog.jQuery()));

