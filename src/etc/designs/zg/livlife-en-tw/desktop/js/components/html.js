/**
 * HTML
 */



/**
 * Carousel
 */
(function ($) {
    "use strict";

    var api = {};

    $(function() {

        function unityTool( a ) {
            a.addClass("box-visible");
            var value = a.parent();
            if( value.hasClass("page-treatment") ) {
            } else {
                unityTool( value );
            }
        }


        function openPrintDialogue(){
            window.print();
        };
        $('.unity-tool .box-footer h3').on('click', openPrintDialogue);
        $(document).ready(function(){
            unityTool($(".snippetReference.unity-tool"));

        });


        /*####################  SET CHARTS DIAGNOSIS  ####################*/

        $('.diagnosis .chart.region_1,.diagnosis .chart.age_1,.diagnosis .chart.gender_1,.diagnosis .chart.sexual_1').easyPieChart({
            barColor: '#e2001d',
            trackColor: '#e9e9e9',
            lineCap: 'round',
            lineWidth: 16,
            onStep: function(from, to, percent) {
                $(this.el).find('.chart-value').text(Math.abs(Math.round(percent)) + '%');
                if(percent == 0) {
                    this.options.barColor = this.options.trackColor;
                } else {
                    this.options.barColor = '#e2001d';
                }
            }
        });

        $('.diagnosis .chart.region_2,.diagnosis .chart.age_2,.diagnosis .chart.gender_2,.diagnosis .chart.sexual_2').easyPieChart({
            barColor: '#126c81',
            trackColor: '#e9e9e9',
            lineCap: 'round',
            lineWidth: 16,
            onStep: function (from, to, percent) {
                $(this.el).find('.chart-value').text(Math.abs(Math.round(percent)) + '%');
                if(percent == 0) {
                    this.options.barColor = this.options.trackColor;
                } else {
                    this.options.barColor = '#126c81';
                }
            }
        });
        $('.diagnosis .chart.age_3,.diagnosis .chart.gender_3,.diagnosis .chart.sexual_3').easyPieChart({
            barColor: '#535353',
            trackColor: '#e9e9e9',
            lineCap: 'round',
            lineWidth: 16,
            onStep: function (from, to, percent) {
                $(this.el).find('.chart-value').text(Math.abs(Math.round(percent)) + '%');
                if(percent == 0) {
                    this.options.barColor = this.options.trackColor;
                } else {
                    this.options.barColor = '#535353';
                }
            }
        });
        $('.diagnosis .chart.sexual_4').easyPieChart({
            barColor: '#898989',
            trackColor: '#e9e9e9',
            lineCap: 'round',
            lineWidth: 16,
            onStep: function (from, to, percent) {
                $(this.el).find('.chart-value').text(Math.abs(Math.round(percent)) + '%');
                if(percent == 0) {
                    this.options.barColor = this.options.trackColor;
                } else {
                    this.options.barColor = '#898989';
                }
            }
        });

        /*####################  SET CHARTS DISCLOSURE  ####################*/

        $('.disclosure .chart.region_1,.disclosure .chart.age_1,.disclosure .chart.gender_1,.disclosure .chart.sexual_1').easyPieChart({
            barColor: '#e2001d',
            trackColor: '#e9e9e9',
            lineCap: 'round',
            lineWidth: 16,
            onStep: function(from, to, percent) {
                $(this.el).find('.chart-value').text(Math.abs(Math.round(percent)) + '%');
                if(percent == 0) {
                    this.options.barColor = this.options.trackColor;
                } else {
                    this.options.barColor = '#e2001d';
                }
            }
        });

        $('.disclosure .chart.region_2,.disclosure .chart.age_2,.disclosure .chart.gender_2,.disclosure .chart.sexual_2').easyPieChart({
            barColor: '#02c1c1',
            trackColor: '#e9e9e9',
            lineCap: 'round',
            lineWidth: 16,
            onStep: function (from, to, percent) {
                $(this.el).find('.chart-value').text(Math.abs(Math.round(percent)) + '%');
                if(percent == 0) {
                    this.options.barColor = this.options.trackColor;
                } else {
                    this.options.barColor = '#02c1c1';
                }
            }
        });
        $('.disclosure .chart.age_3,.disclosure .chart.gender_3,.disclosure .chart.sexual_3').easyPieChart({
            barColor: '#535353',
            trackColor: '#e9e9e9',
            lineCap: 'round',
            lineWidth: 16,
            onStep: function (from, to, percent) {
                $(this.el).find('.chart-value').text(Math.abs(Math.round(percent)) + '%');
                if(percent == 0) {
                    this.options.barColor = this.options.trackColor;
                } else {
                    this.options.barColor = '#535353';
                }
            }
        });
        $('.disclosure .chart.sexual_4').easyPieChart({
            barColor: '#898989',
            trackColor: '#e9e9e9',
            lineCap: 'round',
            lineWidth: 16,
            onStep: function (from, to, percent) {
                $(this.el).find('.chart-value').text(Math.abs(Math.round(percent)) + '%');
                if(percent == 0) {
                    this.options.barColor = this.options.trackColor;
                } else {
                    this.options.barColor = '#898989';
                }
            }
        });

        //main function after user the filter
        function set_chart(chart_name, chart_value) {
            var chart = window.chart = $('.chart.' + chart_name).data('easyPieChart');
            chart.update(chart_value);
            if (chart_value == 0) {
                $('.' + chart_name + ' label').removeClass('active');

            } else {
                $('.' + chart_name + ' label').addClass('active');
            }

            console.log(chart_name,chart_value );
            if (chart_name.indexOf('region') > -1)
            {
                if (chart_value == 0 ) {
                    $('.icon_region').removeClass(chart_name);
                } else {
                    $('.icon_region').addClass(chart_name);
                }
            }
        }

        function set_chart_bar(chart_name, chart_value) {
            $('.chart-bar.' + chart_name + ' span').css("width", chart_value+"%");
            $('.' + chart_name).next('.percent').html(chart_value + '%');

        }

        var charts_values = new Array();
        var charts_bar_values = new Array();

/*####################  DIAGNOSIS  ####################*/


        function diagnosisChartsOne() {
            $('.page-diagnosis .chart-content ul li').hide();
            $('.page-diagnosis .chart-content ul li:nth-child(1)').show();
            charts_values = [];
            charts_values.push(
                {name:"region_1", value:"-68"},
                {name:"region_2", value:"-68"},
                {name:"age_1", value:"-76"},
                {name:"age_2", value:"-70"},
                {name:"age_3", value:"-60"},
                {name:"gender_1", value:"-70"},
                {name:"gender_2", value:"-63"},
                {name:"gender_3", value:"-43"}
                );
                for (var i=0; i < charts_values.length; i++)
                    set_chart(charts_values[i]['name'], charts_values[i]['value']);

            charts_bar_values = [];
            charts_bar_values.push(
                {name:"sexual_1", value:"65"},
                {name:"sexual_2", value:"70"},
                {name:"sexual_3", value:"62"},
                {name:"sexual_4", value:"38"}
            );

            for (var z=0; z < charts_bar_values.length; z++)
                set_chart_bar(charts_bar_values[z]['name'], charts_bar_values[z]['value']);

            $('.diagnosis__filters input[type=checkbox]').prop('checked',true);
            $('.applied_filters > span').show();

        }

        function diagnosisChartsTwo() {
            $('.page-diagnosis .chart-content ul li').hide();
            $('.page-diagnosis .chart-content ul li:nth-child(2)').show();
            charts_values = [];
            charts_values.push(
                {name:"region_1", value:"-44"},
                {name:"region_2", value:"-47"},
                {name:"age_1", value:"-56"},
                {name:"age_2", value:"-44"},
                {name:"age_3", value:"-38"},
                {name:"gender_1", value:"-51"},
                {name:"gender_2", value:"-28"},
                {name:"gender_3", value:"-14"}
            );
            for (var i=0; i < charts_values.length; i++)
                 set_chart(charts_values[i]['name'], charts_values[i]['value']);

            charts_bar_values = [];
            charts_bar_values.push(
                {name:"sexual_1", value:"27"},
                {name:"sexual_2", value:"54"},
                {name:"sexual_3", value:"28"},
                {name:"sexual_4", value:"54"}
            );
            for (var z=0; z < charts_bar_values.length; z++)
                 set_chart_bar(charts_bar_values[z]['name'], charts_bar_values[z]['value']);

            $('.diagnosis__filters input[type=checkbox]').prop('checked',true);
            $('.applied_filters > span').show();

        }

        function diagnosisChartsThree() {
            $('.page-diagnosis .chart-content ul li').hide();
            $('.page-diagnosis .chart-content ul li:nth-child(3)').show();
            charts_values = [];
            charts_values.push(
                {name:"region_1", value:"-33"},
                {name:"region_2", value:"-32"},
                {name:"age_1", value:"-29"},
                {name:"age_2", value:"-33"},
                {name:"age_3", value:"-35"},
                {name:"gender_1", value:"-34"},
                {name:"gender_2", value:"-27"},
                {name:"gender_3", value:"-14"}
            );
            for (var i=0; i < charts_values.length; i++)
                set_chart(charts_values[i]['name'], charts_values[i]['value']);

            charts_bar_values = [];
            charts_bar_values.push(
                {name:"sexual_1", value:"29"},
                {name:"sexual_2", value:"34"},
                {name:"sexual_3", value:"40"},
                {name:"sexual_4", value:"8"}
            );
            for (var z=0; z < charts_bar_values.length; z++)
                set_chart_bar(charts_bar_values[z]['name'], charts_bar_values[z]['value']);

            $('.diagnosis__filters input[type=checkbox]').prop('checked',true);
            $('.applied_filters > span').show();
        }


/*####################  DISCLOSURE  ####################*/

        function disclosureChartsOne() {
            $('.page-disclosure .chart-content ul li').hide();
            $('.page-disclosure .chart-content ul li:nth-child(1)').show();
            charts_values = [];
            charts_values.push(
                {name:"region_1", value:"-26"},
                {name:"region_2", value:"-27"},
                {name:"age_1", value:"-33"},
                {name:"age_2", value:"-26"},
                {name:"age_3", value:"-23"},
                {name:"gender_1", value:"-26"},
                {name:"gender_2", value:"-29"},
                {name:"gender_3", value:"-0"}
            );
            for (var i=0; i < charts_values.length; i++)
                set_chart(charts_values[i]['name'], charts_values[i]['value']);

            charts_bar_values = [];
            charts_bar_values.push(
                {name:"sexual_1", value:"29"},
                {name:"sexual_2", value:"26"},
                {name:"sexual_3", value:"26"},
                {name:"sexual_4", value:"23"}
            );

            for (var z=0; z < charts_bar_values.length; z++)
                set_chart_bar(charts_bar_values[z]['name'], charts_bar_values[z]['value']);

            $('.disclosure__filters input[type=checkbox]').prop('checked',true);
            $('.applied_filters > span').show();

        }

        function disclosureChartsTwo() {
            $('.page-disclosure .chart-content ul li').hide();
            $('.page-disclosure .chart-content ul li:nth-child(2)').show();
            charts_values = [];
            charts_values.push(
                {name:"region_1", value:"-62"},
                {name:"region_2", value:"-65"},
                {name:"age_1", value:"-62"},
                {name:"age_2", value:"-64"},
                {name:"age_3", value:"-64"},
                {name:"gender_1", value:"-65"},
                {name:"gender_2", value:"-62"},
                {name:"gender_3", value:"-29"}
            );
            for (var i=0; i < charts_values.length; i++)
                set_chart(charts_values[i]['name'], charts_values[i]['value']);

            charts_bar_values = [];
            charts_bar_values.push(
                {name:"sexual_1", value:"58"},
                {name:"sexual_2", value:"65"},
                {name:"sexual_3", value:"72"},
                {name:"sexual_4", value:"54"}
            );
            for (var z=0; z < charts_bar_values.length; z++)
                set_chart_bar(charts_bar_values[z]['name'], charts_bar_values[z]['value']);

            $('.disclosure__filters input[type=checkbox]').prop('checked',true);
            $('.applied_filters > span').show();

        }

        function disclosureChartsThree() {
            $('.page-disclosure .chart-content ul li').hide();
            $('.page-disclosure .chart-content ul li:nth-child(3)').show();
            charts_values = [];
            charts_values.push(
                {name:"region_1", value:"-0"},
                {name:"region_2", value:"-0"},
                {name:"age_1", value:"-0"},
                {name:"age_2", value:"-0"},
                {name:"age_3", value:"-0"},
                {name:"gender_1", value:"-0"},
                {name:"gender_2", value:"-0"},
                {name:"gender_3", value:"-0"}
            );
            for (var i=0; i < charts_values.length; i++)
                set_chart(charts_values[i]['name'], charts_values[i]['value']);

            charts_bar_values = [];
            charts_bar_values.push(
                {name:"sexual_1", value:"0"},
                {name:"sexual_2", value:"0"},
                {name:"sexual_3", value:"0"},
                {name:"sexual_4", value:"0"}
            );
            for (var z=0; z < charts_bar_values.length; z++)
                set_chart_bar(charts_bar_values[z]['name'], charts_bar_values[z]['value']);

            $('.disclosure__filters input[type=checkbox]').prop('checked',true);
            $('.applied_filters > span').show();
        }



        $('.select .label').on('click', function() {
            if ($(this).parent().hasClass('active')) {
                $(this).next().slideUp();
                $(this).parent().removeClass('active');
            } else {
                $(this).next().slideDown();
                $(this).parent().addClass('active');
            }
        });

        //clear each of filters
        $('.applied_filters span span').on('click', function(){
            var target = $(this).parent().attr('class');
            $('.diagnosis__filters #' + target).trigger( "click" );
            $('.applied_filters_clear').show();
        });

        //clear all filters
        $('.applied_filters_clear').on('click', function(){
            $('.diagnosis__filters input[type=checkbox]').removeAttr('checked');
            $('.diagnosis__filters input[type=checkbox]').prop('checked',false);
            //filters_chart();
            $('.applied_filters > span').hide();
            set_chart('region_1', '0');
            set_chart('region_2', '0');
            set_chart('age_1', '0');
            set_chart('age_2', '0');
            set_chart('age_3', '0');
            set_chart('gender_1', '0');
            set_chart('gender_2', '0');
            set_chart('gender_3', '0');
            set_chart_bar('sexual_1', '0');
            set_chart_bar('sexual_2', '0');
            set_chart_bar('sexual_3', '0');
            set_chart_bar('sexual_4', '0');
        });


        function findElement(arr, propName, propValue) {
            for (var i=0; i < arr.length; i++)
                if (arr[i][propName] == propValue)
                    return arr[i];
        };

        $('.diagnosis__filters input[type=checkbox]').on('click', function (event) {
            var chart_name = ($(this).data('name'));

            if($(this).is(':checked')) {
                if (event.target.id.indexOf('sexual') > -1)
                {
                    var y = findElement(charts_bar_values, "name", chart_name);
                    set_chart_bar(chart_name, y["value"]);
                } else {
                    var x = findElement(charts_values, "name", chart_name);
                    set_chart(chart_name, x["value"]);
                }
                $('.applied_filters span.' + chart_name).show();
            } else {
                if (event.target.id.indexOf('sexual') > -1)
                {
                    var y = findElement(charts_bar_values, "name", chart_name);
                    set_chart_bar(chart_name, 0);
                } else {
                    var x = findElement(charts_values, "name", chart_name);
                    set_chart(chart_name, 0);
                }
                $('.applied_filters > span.' + chart_name).hide();
            }

        });



        /*####################  DIAGNOSIS START  ####################*/

        $('.page-diagnosis .charts-start .box').on('click', function (event) {
            if ($(this).hasClass('active')) {
                $('.charts-start .box').removeClass('active');
                $('.chart-content').slideUp(1450);
                set_chart('region_1', '0');
                set_chart('region_2', '0');
                set_chart('age_1', '0');
                set_chart('age_2', '0');
                set_chart('age_3', '0');
                set_chart('gender_1', '0');
                set_chart('gender_2', '0');
                set_chart('gender_3', '0');
                set_chart_bar('sexual_1', '0');
                set_chart_bar('sexual_2', '0');
                set_chart_bar('sexual_3', '0');
                set_chart_bar('sexual_4', '0');
            } else {
                $('.charts-start .box').removeClass('active');
                $(this).addClass('active');
                $('.chart-content').slideDown();
                if($(this).hasClass('start-chart-1')){
                    diagnosisChartsOne();
                } else if($(this).hasClass('start-chart-2')){
                    diagnosisChartsTwo();
                } else if($(this).hasClass('start-chart-3')){
                    diagnosisChartsThree();
                }
            }
        });

        /*####################  DISCLOSURE START  ####################*/

        $('.page-disclosure .charts-start .box').on('click', function (event) {
            if ($(this).hasClass('active')) {
                $('.charts-start .box').removeClass('active');
                $('.chart-content').slideUp(1450);
                set_chart('region_1', '0');
                set_chart('region_2', '0');
                set_chart('age_1', '0');
                set_chart('age_2', '0');
                set_chart('age_3', '0');
                set_chart('gender_1', '0');
                set_chart('gender_2', '0');
                set_chart('gender_3', '0');
                set_chart_bar('sexual_1', '0');
                set_chart_bar('sexual_2', '0');
                set_chart_bar('sexual_3', '0');
                set_chart_bar('sexual_4', '0');
            } else {
                $('.charts-start .box').removeClass('active');
                $(this).addClass('active');
                $('.chart-content').slideDown();
                if($(this).hasClass('start-chart-1')){
                    disclosureChartsOne();
                } else if($(this).hasClass('start-chart-2')){
                    disclosureChartsTwo();
                } else if($(this).hasClass('start-chart-3')){
                    disclosureChartsThree();
                }
            }
        });


    });


    Cog.registerComponent({
        name: "diagnosis",
        api: api,
        selector: ".diagnosis",
        requires: [
            {
                name: "utils.status",
                apiId: "status"
            }
        ]
    });


})(Cog.jQuery());


(function ($) {
    "use strict";

    var api = {};

    $(function() {

        // header minimize after scrolling
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();
            if (scroll >= 300) {
                $("#header").addClass("smallHeader");
            } else {
                $("#header").removeClass("smallHeader");
            }
        });

        //H2 in circle - separate words <br>
        $(".hero-subpageTitle h2").each(function() {
            var html = $(this).html().split(" ");
            var newhtml = [];
            for(var i=0; i< html.length; i++) {
                //if(i>0 && (i%1) == 0)
                    //newhtml.push("<br />");
                newhtml.push("<span class='part-"+[i]+"'>" + html[i] + "</span>");
            }
            $(this).html(newhtml.join(" "));
        });


        //About PP - steering committee

        $('.committe-tile.alpha').on('click',function (e) {
            var main_block = $(this).closest('.person');
            var target = $(main_block).next().find('.person-one');

            if ($(target).hasClass('active')) {
                $(target).slideUp(0).removeClass('active');
                $('.committe-tile').removeClass('active');
            } else {
                $('.person-two, .person-one').slideUp(0).removeClass('active');
                $(target).slideDown(0).addClass('active');
                $('.committe-tile').removeClass('active');
                $(this).addClass('active');
            }

        });
        $('.committe-tile.omega').on('click',function (e) {
            var main_block = $(this).closest('.person');
            var target = $(main_block).next().find('.person-two');
            if ($(target).hasClass('active')) {
                $(target).slideUp(0).removeClass('active');
                $('.committe-tile').removeClass('active');
            } else {
                $('.person-one, .person-two').slideUp(0).removeClass('active');
                $('.committe-tile').removeClass('active');
                $(target).slideDown(0).addClass('active');
                $(this).addClass('active');
            }
        });

        var state = 0;
        var imgPixelsColor = new Array();
        var imgPixelsGrey = new Array();

        function gray(imgObj,z,check,length) {

            console.log("z"+z);
            console.log("check"+check);

            var canvas = document.createElement('canvas');
            var canvasContext = canvas.getContext('2d');

            var imgW = imgObj.width;
            var imgH = imgObj.height;
            canvas.width = imgW;
            canvas.height = imgH;

            canvasContext.drawImage(imgObj, 0, 0);


            if (state <length){
                imgPixelsColor[z] = canvasContext.getImageData(0, 0, imgW, imgH);
                imgPixelsGrey[z] = canvasContext.getImageData(0, 0, imgW, imgH);

                for (var y = 0; y <imgPixelsGrey[z].height; y++) {
                    for (var x = 0; x <imgPixelsGrey[z].width; x++) {
                        var i = (y * 4) * imgPixelsGrey[z].width + x * 4;
                        var avg = (imgPixelsGrey[z].data[i] + imgPixelsGrey[z].data[i + 1] + imgPixelsGrey[z].data[i + 2]) / 3;
                        imgPixelsGrey[z].data[i] = avg;
                        imgPixelsGrey[z].data[i + 1] = avg;
                        imgPixelsGrey[z].data[i + 2] = avg;
                    }
                }

                state++;
            }



            if( check == true){
                canvasContext.putImageData(imgPixelsColor[z],0,0,0,0, imgPixelsColor[z].width, imgPixelsColor[z].height);
                console.log("color");
            }else {
                canvasContext.putImageData(imgPixelsGrey[z], 0, 0, 0, 0, imgPixelsGrey[z].width, imgPixelsGrey[z].height);
                console.log("grey");
            }
            return canvas.toDataURL();
        }




        if (navigator.userAgent.match(/msie/i) || navigator.userAgent.match(/trident/i) ){
            $("body").addClass("ie");

            $(".grayscale").each(function (i) {
                $(this).addClass("index-"+i);
            });

            var imgObj = document.querySelectorAll('.grayscale img');
            for (var i=0; i<imgObj.length;i++){
                imgObj[i].src = gray(imgObj[i],i,false,imgObj.length);
            }


            $(".grayscale").on({
                mouseover: function (e) {

                    var i= $(this).attr('class').match(/\d+$/)[0];
                        imgObj[i].src = gray(imgObj[i], i, true);

                },
                mouseout: function () {
                    var i= $(this).attr('class').match(/\d+$/)[0];
                    imgObj[i].src = gray(imgObj[i], i, false);
                }
            });


        };






    });

        Cog.registerComponent({
            name: "body",
            api: api,
            selector: "body",
            requires: [
                {
                    name: "utils.status",
                    apiId: "status"
                }
            ]
        });


})(Cog.jQuery());