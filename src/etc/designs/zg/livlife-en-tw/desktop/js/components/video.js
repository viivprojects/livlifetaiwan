/**
 * Video
 */

(function ($) {
	"use strict";

	var api = {};
	api.onRegister = function (scope) {


		var $videoplayer = scope.$scope,
			$video = $videoplayer.find("video"),
			features = [
				"playpause",
				"progress",
				"current",
				"duration",
				"tracks",
				"volume",
				"fullscreen"
			];

		$video.mediaelementplayer({
			features: features,
			enableAutosize: false,
			hideVideoControlsOnLoad: true,
			plugins: ["flash"],
			pluginPath: api.external.settings.themePath + "/assets/swf/",
			flashName: "flashmediaelement.swf",
			videoHeight: $video.attr("height")
		});
	};

	Cog.registerComponent({
		name: "videoplayer",
		api: api,
		selector: ".video",
		requires: [
			{
				name: "utils.settings",
				apiId: "settings"
			}
		]
	});

})(Cog.jQuery());


