/**
 * Status - get current page status
 */

(function ($) {
	"use strict";
	var api = {},
		cache = {},
		sharedApi = {};

	sharedApi.isAuthor = function () {
		if(typeof cache.isAuthor !== "undefined") {
			return cache.isAuthor;
		} else {
			return cache.isAuthor = $("body").hasClass("cq-wcm-edit");
		}
	};

	sharedApi.isPublish = function() {
		return !sharedApi.isAuthor();
	}


	Cog.registerStatic({
		name: "utils.status",
		api: api,
		sharedApi: sharedApi
	});

})(Cog.jQuery());