(function ($) {
	"use strict";

	var api = {},
		href,
		hiddenClass = "is-hidden";

	function openDialog(event) {
        var $target = $(event.target);
        event.preventDefault();
        if ($target.hasClass("external")) {
            href = $target.attr("href");
        } else {
            href = $target.closest(".external").attr("href");
        }
		api.scope.removeClass(hiddenClass);
		return false;
	}

	function acceptButton() {
		window.location.href = href;
	}

	function denyButton() {
		api.scope.addClass(hiddenClass);
	}

	api.init = function (scope) {
		api.scope = scope;
		api.scope.find(".exit-notification-accept").click(acceptButton);
		api.scope.find(".exit-notification-deny").click(denyButton);
		$("body").on("click", "a.external", openDialog);
	};

	Cog.register({
		name: "exitNotification",
		api: api,
		selector: "#exit-notification"
	});

})(Cog.jQuery());