/**
 * Utils - URL - url modification helpers
 */

(function ($) {
	"use strict";
	var api = {},
		sharedApi = {};

	/* @CQ.shared.HTTP.setSelector */
	sharedApi.setSelector = function (url, selector, index) {

		var post = "";
		var pIndex = url.indexOf("?");
		if (pIndex === -1) {
			pIndex = url.indexOf("#");
		}
		if (pIndex !== -1) {
			post = url.substring(pIndex);
			url = url.substring(0, pIndex);
		}

		index = index ? index : 0;

		var selectors = sharedApi.getSelectors(url);
		var ext = url.substring(url.lastIndexOf("."));
		// cut extension
		url = url.substring(0, url.lastIndexOf("."));
		// cut selectors
		var fragment = (selectors.length > 0) ? url.replace("." + selectors.join("."), "") : url;

		if (selectors.length > 0) {
			for (var i = 0; i < selectors.length; i++) {
				if (index === i) {
					fragment += "." + selector;
				} else {
					fragment += "." + selectors[i];
				}
			}
		} else {
			fragment += "." + selector;
		}

		return fragment + ext + post;
	};

	/* @CQ.shared.HTTP.getSelectors */
	sharedApi.getSelectors = function (url) {
		var selectors = [];
		url = url || window.location.href;
		url = sharedApi.removeParameters(url);
		url = sharedApi.removeAnchor(url);
		var fragment = url.substring(url.lastIndexOf("/"));
		if (fragment) {
			var split = fragment.split(".");
			if (split.length > 2) {
				for (var i = 0; i < split.length; i++) {
					// don't add node name and extension as selectors
					if (i > 0 && i < split.length - 1) {
						selectors.push(split[i]);
					}
				}
			}
		}

		return selectors;
	};

	/* @_g.HTTP.removeParameters */
	sharedApi.removeParameters = function (url) {
		if (url.indexOf("?") !== -1) {
			return url.substring(0, url.indexOf("?"));
		}
		return url;
	};

	/* @_g.HTTP.removeAnchor*/
	sharedApi.removeAnchor = function (url) {
		if (url.indexOf("#") !== -1) {
			return url.substring(0, url.indexOf("#"));
		}
		return url;
	};

	Cog.registerStatic({
		name: "utils.url",
		api: api,
		sharedApi: sharedApi
	});

})(Cog.jQuery());