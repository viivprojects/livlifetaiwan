var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var compass = require('compass-importer');
var autoprefixer = require('gulp-autoprefixer');
//var concat = require('gulp-concat');
var gutil = require('gulp-util');
var watch = require('gulp-watch');
var sourcemaps = require('gulp-sourcemaps');
gulp.task('default', ['compile-sass', 'serve', 'watch']);
gulp.task('dev', ['compile-sass-short', 'serve', 'watch']);
gulp.task('serve', function () {
    browserSync.init({
        server: {
            baseDir: "./"
        }
        , startPath: "src/content/cf-viiv/livlife/en_TW/home.html"
    });
    gulp.watch('src/etc/designs/zg/livlife-en-tw/desktop/sass/**/*.scss', ['sass-watch']);
});
gulp.task('compile-sass', function () {
    return gulp.src('src/etc/designs/zg/livlife-en-tw/desktop/sass/**/*.scss')
        .pipe(plumber({errorHandler: onError}))
        .pipe(sass({importer: compass, outputStyle: 'compact'}).on('error', function (error) {
            gutil.log('\nline: ' + error.line + '\ncolumn: ' + error.column + '\n' + error.message);
            gutil.log('SASS BUILD FAILED');
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('src/etc/designs/zg/livlife-en-tw/desktop/css')).on('end', function () {
            gutil.log('SASS LOCAL + MAP BUILD FINISHED');
        });
});
gulp.task('compile-sass-short', function () {
    return gulp.src('src/etc/designs/zg/livlife-en-tw/desktop/sass/**/*.scss')
        .pipe(plumber({errorHandler: onError}))
        .pipe(sass({importer: compass, outputStyle: 'compact'}).on('error', function (error) {
            gutil.log('\nline: ' + error.line + '\ncolumn: ' + error.column + '\n' + error.message);
            gutil.log('SASS BUILD FAILED');
        }))
        .pipe(autoprefixer())
        .pipe(gulp.dest('src/etc/designs/zg/livlife-en-tw/desktop/css')).on('end', function () {
            gutil.log('SASS DEVELOPMENT BUILD FINISHED');
        });
});
gulp.task('browser-sync-reload', function () {
    browserSync.reload();
});
gulp.task('watch', function () {
    gulp.watch('src/etc/designs/zg/livlife-en-tw/desktop/sass/**/*.scss', ['sass-watch']);
});
gulp.task('sass-watch', ['compile-sass'], function (done) {
    browserSync.reload();
    done();
});
var onError = function (err) {
    gutil.log(gutil.colors.red(err.toString()));
    this.emit('end');
};

